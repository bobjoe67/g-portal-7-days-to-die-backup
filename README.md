# g-portal 7 Days to Die backup

This repository contains a script to backup your www.g-portal.com hosted 7 Days to Die server save with a CI/CD pipeline.

This works by using the FTP data generated when a server is created on g-portal, the FTP information is provided to you for doing your own modding and backups.

*This repository can possibly be modified for other games, however I only know the structure of 7 Days to Die*

This will backup the save file for the server, and keep it here in gitlab for 10 days. (This can be changed in the .gitlab-ci.yml file, however you may run into quota limits)

## Setup

If you would like to use this with your server you should sign up for Gitlab.com. The free tier is enough to use this repo.

### Fork

To begin, you need to fork this repository for your account. Go to the **Project overview** > **Details** page and press *Fork* in the upper right corner.

### Set your CI/CD variables

Once your fork is created, you need to setup your FTP CI/CD variables from the **Settings** > **CI/CD** UI
Scroll down to the *Variables* section and press *Expand* and *Add Variable*

You must set all of the following variables

| Variable     | Description |
| FTP_USERNAME | The ftp username generated by g-portal for your server |
| FTP_PASSOWRD | The ftp password generated by g-portal for your server |
| FTP_IP       | The Hostname / IP address of your g-portal server |
| FTP_PORT     | The ftp port of your g-portal server | 

#### A note about security
By default, this pipeline will run on a public runner, and is the easiest way to get your backups running, however, running jobs on a public runner poses an inherent security risk, as those runners are obviously not under your control, and you have no idea what data they can see or are capturing.

The nature of gitlab jobs is to set CI/CD variable as environment variables while the job is run, a public runner COULD theoretically sniff these values. Use public runners at your own risk!

If this sounds scary, you can setup your own runner, but it needs to be online when your pipeline is scheduled to run.

#### Another more IMPORTANT note about security
You should mark your repository as **PRIVATE**, if you leave it public, anyone can download your artifacts! Your backups contain your server password!
You can mark your repository as private from the **Settings** > **General** page.

Scroll to the section titled *Visibility, project features, permissions*
And change your repository visibility to **private**.

### Scheduling your pipeline
Immediately when you create your fork you'll notice a pipeline runs and fails, this is normal as pipelines run anytime sourcecode changes (creation counts as a change!)

After you set your CI/CD variables, you can run your pipeline on-demand from the **CI/CD** > **Pipelines** Page, this creates a backup every time this is run.

You probably don't want to do this manually all the time, so you can schedule this pipeline to run.

Go to the **CI/CD** > **Schedules** page to create a new scheduled run of this pipeline.

Press *New Schedule* and fill out the data for your pipeline, select a preset time to run, or create a custom CRON pattern for your backups.

Make sure *Active* is checked and press *Save pipeline schedule* and you're good to go! The pipeline will run on the schedule supplied.

A daily backup is probably enough for a server with low traffic, but any schedule can be created depending on your needs.

#### Performance
I'm unsure that this affects performance very much, FTP should not lock the cpu during transmission, and since this is a read-only operation, this can be done while the server is LIVE.

I would still recommend scheduling this when traffic is typically low.

## Restoring to a backup
If something happens and you want to restore to a backup, you will need an FTP program on your machine.

This assumes you're using Windows but any FTP client should work.

### Downloading & extract the backup
First you'll need to download the backup, goto the **CI/CD** > **Pipelines** Page and select a recent pipeline run.

On the pipeline screen, select the backup job, and if the artifact is not expired (default 10 days) you can download the entire thing as a zip file.

Extract the downloaded zip to a folder somewhere.

### Prepare your server
In the g-portal website, goto your server's **Basic Settings** page, here you want to change your **Game name** to something other than your original game name, it doesn't matter what you pick, as long as you change it from your live server save game.

Next, restart your server. This might take some time if you don't have a save with the name chosen in the previous step already. This is necessary to unlock the files used by your server, and there is no way to start your server without 7DaysToDie running. 
**If you fail to do this, you will not be able to over-write the files on your server.**

### Connect with an FTP program
Using the information supplied by g-portal which you also setup with your repository, connect to your server. If you are using WinSCP on windows, you can register the link provided by g-portal with WinSCP, allowing you to automattically connect by clicking the IP address in the g-portal website.

### Upload the backup
Open the **remote** saves folder in your FTP program, and open the **local** saves folder extracted from the backup you downloaded. Drag the contents from **local** to **remote**. This will begin the upload process.

When the upload is complete you can close your FTP client.

### Restore the server config
The last step is to change your game name back to what it was from before. Do this and restart your server, and you're successfully restored!
